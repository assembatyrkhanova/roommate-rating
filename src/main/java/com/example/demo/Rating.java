package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Rating {
    public Rating() {

    }



    public Rating(String roommateId, Integer rating) {

        this.roommateId = roommateId;

        this.rating = rating;

    }



    private String roommateId;

    private Integer rating; // 0 to 5
}

